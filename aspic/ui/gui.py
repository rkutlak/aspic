import logging

import PyQt5.QtGui as QtGui
import PyQt5.QtCore as QtCore
import PyQt5.QtWidgets as QtWidgets

from ui.main_window import Ui_MainWindow
from controller import Controller
from graph.qtgraph import GraphWidget, Graph


def get_log():
    return logging.getLogger(__name__)


class AspicMainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        QtWidgets.QMainWindow.__init__(self, parent, f)
        self.setupUi(self)
        # file menu
        self.actionNew.triggered.connect(self.handle_new)
        self.actionOpen.triggered.connect(self.handle_open)
        self.actionSave.triggered.connect(self.handle_save)
        self.actionPrint.triggered.connect(self.handle_print)
        self.actionClose.triggered.connect(self.handle_new)
        # edit menu
        self.actionUndo.triggered.connect(self.handle_undo)
        self.actionRedo.triggered.connect(self.handle_redo)
        # graph menu
        self.actionZoom_In.triggered.connect(self.handle_zoom_in)
        self.actionZoom_Out.triggered.connect(self.handle_zoom_out)
        self.actionOriginal_Size.triggered.connect(self.handle_normal_size)
        self.actionFit_Width.triggered.connect(self.handle_fit_width)
        self.actionFit_to_Window.triggered.connect(self.handle_fit_to_window)
        self.actionPrintGraph.triggered.connect(self.handle_print_graph)
        # line edits
        self.knowledgebaseLine.returnPressed.connect(self.handle_kb_command)
        self.dialogLine.returnPressed.connect(self.handle_dialog_speech_act)
        # some class vars
        self.controller = Controller()
        self.update_menu()
        self.splitter_2.setSizes((0, 500))
        self.handle_new()

# handles for file menu
    def handle_new(self):
        """ Create a new doc. """
        self.handle_close()
        self.handle_load('')

    def handle_open(self):
        dir = (QtWidgets.QFileDialog.
                getOpenFileName(self,
                                'Open Knowledgebase File',
                                './examples',
                                'KB file (*.aspic *.kb *.txt)'))
        if '' != dir[0]:
            self.handle_load(dir[0])

    def handle_load(self, path):
        get_log().warning('loading ' + str(path))
        try:
            res = self.controller.open_file(path)
        except Exception as ex:
            if path == '':
                msg = 'Could not create new file:\n\t%s' % str(ex)
            else:
                msg = 'Could not open file "%s":\n\t%s' % (path[0], str(ex))
            res = '<p style="color:red;">%s</p>' % msg
            self.frame.setWidget(None)
            get_log().exception('Exception %s' % str(ex))
        self.dialogText.append(res)
        self.update_menu()
        self.update_status()

    def handle_save(self):
        get_log().debug('save')
        path = self.controller.dialog.kb.name
        if not path:
            dir = QtWidgets.QFileDialog.getSaveFileName(self,
                                                    'Save Knowledgebase File',
                                                    QtCore.QDir.currentPath())
            if '' != dir[0]: path = dir[0]
        resp = self.controller.save_file(path)
        self.dialogText.append(str(resp))

    def handle_print(self):
        get_log().debug('print')

    def handle_close(self):
        get_log().debug('close')
        ret = QtWidgets.QMessageBox.Discard
        if self.controller.dirty:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setText('The document has been modified.')
            msgBox.setInformativeText('Do you want to save your changes?')
            msgBox.setStandardButtons(QtWidgets.QMessageBox.Save |
                                      QtWidgets.QMessageBox.Discard |
                                      QtWidgets.QMessageBox.Cancel)
            msgBox.setDefaultButton(QtWidgets.QMessageBox.Save)
            ret = msgBox.exec_()
            get_log().debug(ret)

        if ret == QtWidgets.QMessageBox.Save:
            self.handle_save()
        elif ret == QtWidgets.QMessageBox.Cancel:
            return False
        self.clear()
        return True

    def closeEvent(self, e):
        get_log().debug('Triggered "quit"')
        if self.handle_close():
            e.accept()
        else:
            e.ignore()

# handles for edit menu

    def handle_undo(self):
        get_log().debug('undo')

    def handle_redo(self):
        get_log().debug('redo')

    def handle_kb_command(self):
        cmd = self.knowledgebaseLine.text()
        self.knowledgebaseLine.setText('')
        try:
            resp = self.controller.parse_command(cmd)
        except Exception as e:
            resp = '<p style="color:red;">{msg}</p>'.format(msg=str(e))
            get_log().exception(str(e))
        self.dialogText.append(str(resp))
        self.update_status()
        get_log().debug(cmd)
        self.setWindowModified(self.controller.dirty)

    def handle_dialog_speech_act(self):
        cmd = self.dialogLine.text()
        self.dialogLine.setText('')
        self.dialogText.append(cmd)
        try:
            resp = self.controller.parse_command(cmd)
        except Exception as e:
            resp = '<p style="color:red;">{msg}</p>'.format(msg=str(e))
            get_log().exception(str(e))
        self.dialogText.append(str(resp))
        self.update_status()
        get_log().debug(cmd)
        self.setWindowModified(self.controller.dirty)

# handles for graph menu
    def handle_zoom_in(self):
        factor = 1.2
        self.controller.graph_widget.scaleView(factor)

    def handle_zoom_out(self):
        factor = 1/1.2
        self.controller.graph_widget.scaleView(factor)

    def handle_normal_size(self):
        self.controller.graph_widget.scaleView(1.0)

    def handle_fit_width(self):
        self.controller.graph_widget.fitToWidith(self.frame.width()-20)

    def handle_fit_to_window(self):
        checked = self.actionFit_to_Window.checked()
        self.controller.graph_widget.fitToWidith(self.frame.width()-20)
        self.actionZoom_In.setEnabled(not checked)
        self.actionZoom_Out.setEnabled(not checked)
        self.actionOriginal_Size.setEnabled(not checked)
        self.actionFit_Width.setEnabled(not checked)

    def handle_print_graph(self):
        dir = QtWidgets.QFileDialog.getSaveFileName(self, 'Open File',
                                                    QtCore.QDir.currentPath())
        if '' != dir[0]:
            self.controller.graph_widget.savePDF(dir[0])
            self.dialogText.append('Graph saved in "%s"' % dir[0])

    def update_menu(self):
        doc_loaded = self.controller.dialog is not None
        self.actionFit_to_Window.setEnabled(doc_loaded)
        self.actionZoom_In.setEnabled(doc_loaded)
        self.actionZoom_Out.setEnabled(doc_loaded)
        self.actionOriginal_Size.setEnabled(doc_loaded)
        self.actionFit_Width.setEnabled(doc_loaded)
        self.actionPrint.setEnabled(False)
        self.actionPrintGraph.setEnabled(doc_loaded)
        self.knowledgebaseLine.setEnabled(doc_loaded)
        self.dialogLine.setEnabled(doc_loaded)

# overriden class methods

    def resizeEvent(self, event):
        super().resizeEvent(event)
        if self.actionFit_to_Window.isChecked():
            self.controller.graph_widget.fitToWidith(self.frame.width() - 20)

# other methods

    def update_status(self):
        """ Update the status of the graph
        depending on the discussion labelling.

        """
        self.frame.setWidget(self.controller.graph_widget)
        labelling = self.controller.get_labelling()
        if not labelling:
            get_log().warning('no labelling')
            return
        get_log().debug('updating colours')

        get_log().debug('labelling IN:')
        for a in labelling.IN:
            self.controller.graph_widget.setColour(a, 'green')
            get_log().debug(a)

        get_log().debug('labelling OUT:')
        for a in labelling.OUT:
            self.controller.graph_widget.setColour(a, 'red')
            get_log().debug(a)

        get_log().debug('labelling UNDEC:')
        for a in labelling.UNDEC:
            self.controller.graph_widget.setColour(a, 'lightgray')
            get_log().debug(a)

    def clear(self):
        """ Clear the text boxes, etc. """
        self.frame.setWidget(GraphWidget(Graph()))
        self.dialogLine.setText('')
        self.dialogText.setText('')
        self.knowledgebaseLine.setText('')
#        self.ruleListView.clear()
#        self.argumentListView.clear()
