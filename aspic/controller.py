
from graph.qtgraph import GraphWidget, Graph
from argumentation.dialog import Dialog


class Controller:
    def __init__(self):
        self.dialog = Dialog()
        self.graph_widget = None
        self.history = None
        self.dirty = False

    def new_file(self):
        self.dialog = Dialog()
        self.graph_widget = create_graph(self.dialog)
        self.history = []
        self.dirty = False
        return 'New file created.'

    def open_file(self, path):
        if path == '': return self.new_file()
        self.dialog = Dialog(path)
        self.graph_widget = create_graph(self.dialog)
        self.history = []
        self.dirty = False
        return 'File "%s" loaded.' % path

    def save_file(self, path):
        if path == '': path = 'tmp.kb'
        self.dialog.kb.save_into_file(path)
        self.dirty = False
        return 'File saved as "%s".' % path

    def get_labelling(self):
        try:
            self._check_init()
            return self.dialog.labelling
        except:
            return None

    def parse_command(self, command):
        """ Parse a command and return the result. """
        self._check_init()
        print('parsing command "%s"' % command)
        tmp = dict()
        for k, v in self.graph_widget.nodes.items():
            tmp[k] = (v.pos().x(), v.pos().y(), v.forceLayout)
        res = self.dialog.parse(command)
#        update_graph(self.dialog, self.graph_widget)
        # save the coordinates
        self.graph_widget = create_graph(self.dialog)
        # restore them in the new graph
        for k, v in self.graph_widget.nodes.items():
            if k in tmp:
                v.setPos(tmp[k][0], tmp[k][1])
                v.forceLayout = tmp[k][2]
            else:
                print('"%s" not in new widget' % str(k))
        # TODO: this is too inclusive (ie, it will set dirty when asking 'why')
        if res: self.dirty = True
        return res

    def _check_init(self):
        """ Check that the controller has an initialised dialog.
        If the controlloer does not have a currently opened dialog, raise exc.

        """
        if not self.dialog:
            raise AspicException('No file opened')


def create_graph(dialog):
    """ Create a new graph from a dialog. """
    args = list(dialog.aaf.arguments)
    widget = GraphWidget(Graph(),
                         'Argumentation Framework: %s' % dialog.kb.name)
    print('creating graph')
    print('args:' + str(args))
    for arg in args:
        print('\tadding arg {0} into the widget'.format(arg))
        widget.createNode(arg)
    for arg in args:
        for arg2 in arg.plus:
            widget.createEdge( (arg, arg2) )
    widget.autoLayout()
    widget.forceLayout(True)
    return widget

def update_graph(dialog, widget):
    """ Update the widget given the graph in the dialog. """
    args = list(dialog.aaf.arguments)
    for arg in args:
        if arg not in widget.nodes:
            widget.createNode(arg)

    for arg in args:
        for arg2 in arg.plus:
            if (arg, arg2) not in widget.edges:
                widget.createEdge( (arg, arg2) )

    # TODO: remove old nodes that are not in args
    to_delete = []
    for name, node in widget.nodes.items():
        if name not in args:
            to_delete.append(name)
    for n in to_delete:
        widget.removeNode(n)
    return widget
