#############################################################################
##
## Copyright (C) 2013 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy Aspic- Program.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################


# Code from:
#   http://victorlin.me/posts/2012/08/26/good-logging-practice-in-python
# format fields:
#   http://docs.python.org/3.3/library/logging.html#logrecord-attributes

import os
import sys
import sip
import logging
import inspect
import logging.config

import yaml

def setup_logging(
    default_path='log.config.yaml',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    """Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.load(f.read())
        logging.config.dictConfig(config)
        print('Using log config file "%s"' % path)
    else:
        print('Could not open log config file "%s"' % path)
        logging.basicConfig(level=default_level)


# read the config file
if getattr(sys, 'frozen', False): # frozen
    mod_path = os.path.dirname(sys.executable)
else: # unfrozen
    mod_path = os.path.dirname(os.path.realpath(__file__))
    mod_path += os.path.sep + '..' + os.path.sep + 'resources'

config_path = mod_path + os.path.sep + 'log.config.yaml'
setup_logging(config_path)


from PyQt5.QtWidgets import QApplication


sys.path.append('sassyargumentation')
sys.path.append('sassygraph')


from ui.gui import AspicMainWindow
from argumentation.dialog import Commands

# run the graphical user interface
def run_gui():
    logging.basicConfig(level=logging.DEBUG)
    app = QApplication(sys.argv)
    window = AspicMainWindow()
    window.show()
    sys.exit(app.exec_())


# run the command line interface
def run_cli():
    app = Commands()
    app.cmdloop()


if __name__ == "__main__":
    run_gui()
