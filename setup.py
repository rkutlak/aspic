"""
Usage:
    python3 setup.py py2app
"""

try:
    from cx_Freeze import setup, Executable
except:
    from setuptools import setup, find_packages

import io
import codecs
import os
import sys

base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

APP = ['aspic/main.py']
DATA_FILES = []
# py2app options
APPOPTIONS = {'argv_emulation': True}
# cx_Freeze options
EXEOPTIONS = {'includes': 'atexit'}
EXECUTABLES = []
try:
    EXECUTABLES = [ Executable('aspic/main.py', base=base) ]
except:
    pass

here = os.path.abspath(os.path.dirname(__file__))

def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)

#long_description = read('README.txt', 'CHANGES.txt')
long_description = """
The program Aspic- developed in the project
Scrutable Autonomous Systems (SAsSy)
is an implementation of instantiated argumentation
framework with unrestricted rebut.

See papers by Caminada and Podlaszevski.

"""


setup(
    name='Aspic',
    version=0.1,
    url='http://bitbucket.org/rkutlak/aspic',
    license='BSD',
    app=APP,
    executables=EXECUTABLES,
    data_files=DATA_FILES,
    options={'py2app': APPOPTIONS,
             'build_exe': EXEOPTIONS
             },
    author='Roman Kutlak',
    install_requires=['pyyaml>=1.0'
                      ],
#    tests_require=['pytest'],
   setup_requires=['py2app'],
#    extras_require={
#        'testing': ['pytest'],
#    },
#    cmdclass={'test': PyTest},
    author_email='roman@kutlak.net',
    description='Formal argumentation app.',
    long_description=long_description,
    packages=['aspic'],
    include_package_data=True,
    platforms='any',
#    test_suite='aspic.test.main',
    classifiers = [
        'Programming Language :: Python',
        'Development Status :: 1 - Alpha',
        'Natural Language :: English',
#        'Environment :: Web Environment',
#        'Intended Audience :: Developers',
#        'License :: OSI Approved :: Apache Software License',
#        'Operating System :: OS Independent',
#        'Topic :: Software Development :: Libraries :: Python Modules',
#        'Topic :: Software Development :: Libraries :: Application Frameworks',
#        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        ]
)
