Aspic- is a python implementation of the instantiated grounded discussion.

The program is wirtten in python (version 3.3) and uses the folling libs:
    PyQt5      - for GUI
    pyparsing  - parsing argumentation KB
    yaml       - parsing logging configuration

The full project is split into 3 git repositories:
    https://bitbucket.org/rkutlak/aspic - the main project
    https://bitbucket.org/rkutlak/sassyargumentation - the argumentation lib
    https://bitbucket.org/rkutlak/sassygraph - graphics lib
    
To download the necessary packages use the following git commands:
    git clone https://rkutlak@bitbucket.org/rkutlak/aspic.git
    cd aspic
    git submodule init
    git submodule update
    # sometimes the modules are not updated - do that manually
    cd ./sassygraph
    git checkout master
    git pull
    cd ../sassyargumentation
    git checkout master
    git pull
    cd ..
    
And run the program:
    python3 aspic/main.py

Usage:
You can create new arguments by adding them to the KB 
using one of the command lines:
	e.g., 
	assert ==> foo   // this will create a defeasible rule concluding 'foo'
	assert --> bar   // this will create a strict rule concluding 'bar'
	assert foo, bar ==> baz
	assert --> -foo
	assert --> -baz
	
You can retract rules, in which case you have to use the same format:
	retract --> -foo
	
You can print the KB:
	print kb

You can ask about the status of arguments using their conclusions:
	why baz        // why is 'baz' concluded
	why in baz     // why is 'baz' labelled IN
	why out baz    // why is 'baz' labelled OUT

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

* Installation: *

I. Download and install Python 3
    - for simplicity use the official binary for your system downloaded from
        http://python.org/getit
    - the source is being developed on Python version 3.3 on Mac
    
II. Download and install Qt5
    - again, binary from http://qt-project.org/ should be sufficient
    - the source is being developed using Qt 5
        
III. Download and install PyQt5
    - here a source installation is required
    - code available at www.riverbankcomputing.co.uk/software/pyqt/download
    A. First download and install SIP (the latest version)
        - extract the archive and inside the sip folder run:
        python3 configure.py
        make
        sudo make install

    B. Download and install PyQt5 from source
        - again extract the downloaded source code and inside the folder run:
        python3 configure.py
        make
        sudo make install

    This will take a while to compile, PyQt should automatically use the Qt 
    version installed by default in /Applications
        
    In case the configure script cannot find Qt or SIP, pass the path as a
    parameter: python3 configure.py --qmake=/path/to/qmake --sip=/path/to/sip

    
IV. PyParsing
    https://pypi.python.org/pypi/pyparsing
    Same as rdflib, you should be able to install pyparsing

You should now have all the necessary packages installed in your python3 dist.

You can test whether individual packages work by running python3 and importing 
one package at a time (e.g., import PyQt5). No errors should occur.


-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

* Knowledge Base Format *

Below is an example of a knowledge base file:
################################################################################
--> jw
--> mw
--> sw
mt, st --> -jt
jt, mt --> -st
jt, st --> -mt

R1: jw ==> jt
R2: mw ==> mt
sw ==> st

# preference:

R2 < R1 

=(-foo)=> bar # unless foo, assume bar - if 'foo' is not asserted, assert 'bar'
################################################################################

