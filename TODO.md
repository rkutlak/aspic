Implement saving and restoring orderings.
Allow adding/removing orderings on the command line.
Print orderings as a part of the knowledge base.

Implement more unit tests.
Implement checks:
    check closure under transposition
    check total ordering
        - transitivity
        - reflexiveness
    check consistency (no oposites inferred)

adapt the dialog
arg                  - INFO     - parsing command: "why out bar"
Traceback (most recent call last):
  File "/Users/roman/Work/software/Aspic/aspic/ui/gui.py", line 140, in handle_dialog_speech_act
    resp = self.controller.parse_command(cmd)
  File "/Users/roman/Work/software/Aspic/aspic/controller.py", line 48, in parse_command
    res = self.dialog.parse(command)
  File "sassyargumentation/argumentation/dialog.py", line 303, in parse
    return self.do_question(tmp[1], tmp[2])
  File "sassyargumentation/argumentation/dialog.py", line 167, in do_question
    arg = self.find_argument(conclusion)
  File "sassyargumentation/argumentation/dialog.py", line 124, in find_argument
    if not conclusion in self.kb._arguments: