
USER=roman
SHELL=/bin/bash
# Note that everyone will have a different path.
#  Regardless of that, it should contain path to the python command and PyQt.
export PATH := /Library/Frameworks/Python.framework/Versions/3.3/bin:$(PATH)

# name of the package
package = aspic
app     = Aspic.app

OPTIONS =  --resources $(package)/log.config.yaml

# default target - run the app from place
debug: structure
	cd tmp && \
	python3 setup.py py2app -A $(OPTIONS)

release: structure
	cd tmp && \
	python3 setup.py py2app $(OPTIONS) && \
	cd dist/$(app)/Contents/ && \
	cp -R /Developer/Qt/5.3/clang_64/plugins/ PlugIns
	macdeployqt tmp/dist/$(app)

# autogenerate basic setup file -- prefer the one in main package
setup: structure
	cd tmp && \
	py2applet --make-setup $(package)/main.py

# create a new copy of the package
structure: clean
	mkdir tmp
	cp -R $(package) tmp/$(package)
	cp -R resources tmp/resources
	cp -R sassygraph/graph tmp/$(package)/
	cp -R sassyargumentation/argumentation tmp/$(package)/
	cp setup.py tmp/

run:
	open tmp/dist/$(app)

all: release

.PHONY : clean
clean:
	rm -rf tmp


################################################################################

# this target does some syntax checking
build:
	pyflakes $(source_files)


print_source_files:
	echo $(source_files)

print_bytecode_files:
	echo $(bytecode_files)


# remove precompiled files
#.PHONY : clean
#clean :
#	-rm -rf src/__pycache__
#	-rm -f $(bytecode_files)


# Xcode and make docs:
# https://developer.apple.com/library/mac/#documentation/DeveloperTools/Reference/XcodeBuildSettingRef/1-Build_Setting_Reference/build_setting_ref.html#//apple_ref/doc/uid/TP40003931-CH3-SW45
